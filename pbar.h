/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_PBAR_H
#define	__GFX_PBAR_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "dsp.h"
#include "tileset.h"

// Progress Bar Tiles
#define	GFX_PBAR_TILE_L																0
#define	GFX_PBAR_TILE_C																1
#define	GFX_PBAR_TILE_R																2
#define	GFX_PBAR_TILE_FL															3
#define	GFX_PBAR_TILE_FC															4
#define	GFX_PBAR_TILE_FR															5

// Default Progress Bar Tileset Arguments
#define	GFX_PBAR_ARGS																GFX_PBAR_TILE_L, GFX_PBAR_TILE_C, GFX_PBAR_TILE_R, GFX_PBAR_TILE_FL, GFX_PBAR_TILE_FC, GFX_PBAR_TILE_FR

// Shortcut - Draw Progress Bar using default tile layout
#define gfx_pbar_draw_default(dsp, tlst, pct, w, x, y)								gfx_pbar_draw(dsp, tlst, pct, w, x, y, GFX_PBAR_ARGS)

// Draw Progress Bar
extern void gfx_pbar_draw(struct gfx_dsp *dsp, struct gfx_tileset *tlst, uint8_t pct, uint16_t w, int x, int y, uint16_t l, uint16_t c, uint16_t r, uint16_t fl, uint16_t fc, uint16_t fr);

#endif
