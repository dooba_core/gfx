/* Dooba SDK
 * Generic Graphics Framework
 */

// External Includes
#include <string.h>
#include <util/cprintf.h>

// Internal Includes
#include "txt.h"

// Text Buffer
char gfx_txt_buf[GFX_TXT_BUFSIZE];

// Draw Text (Skip Lines)
void gfx_txt_s(struct gfx_dsp *dsp, void *s, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col)
{
	// Draw Text
	gfx_txt_s_f(dsp, x, y, w, h, skip_lines, font, col, "%s", s);
}

// Draw Text (Skip Lines) - Safe
void gfx_txt_s_n(struct gfx_dsp *dsp, void *s, uint16_t l, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col)
{
	// Draw Text
	gfx_txt_s_f(dsp, x, y, w, h, skip_lines, font, col, "%t", s, l);
}

// Draw Text (Skip Lines) - Format String
void gfx_txt_s_f(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col, void *fmt, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, fmt);

	// Draw Text
	gfx_txt_s_v(dsp, x, y, w, h, skip_lines, font, col, fmt, ap);

	// Release Args
	va_end(ap);
}

// Draw Text (Skip Lines) - va_list
void gfx_txt_s_v(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col, void *fmt, va_list ap)
{
	// Draw Text
	gfx_txt_s_vp(dsp, x, y, w, h, skip_lines, font, col, fmt, &ap);
}

// Draw Text (Skip Lines) - va_list*
void gfx_txt_s_vp(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col, void *fmt, va_list *ap)
{
	uint16_t txt_len;
	struct gfx_txt_data data;

	// Setup Data
	data.col = col;

	// Prepare Text Buffer
	txt_len = cvpsnprintf(gfx_txt_buf, GFX_TXT_BUFSIZE, fmt, ap);

	// Draw
	gfx_txt_run(dsp, x, y, w, h, skip_lines, font, gfx_txt_buf, txt_len, &data, (gfx_txt_char_cb_t)gfx_txt_char_cb);
}

// Draw Text Character Callback
void gfx_txt_char_cb(struct gfx_txt_data *data, struct gfx_dsp *dsp, int x, int y, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t idx, uint16_t px, uint16_t py, uint16_t line_idx)
{
	// Check Character Code
	if(s[idx] <= ' ')															{ return; }

	// Draw
	gfx_font_draw(dsp, font, s[idx], x + (px * font->fw), y + (py * font->fh), data->col);
}

// Compute coordinates for index (returns 1 if not in area)
uint8_t gfx_txt_itoc(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t idx, uint16_t *px, uint16_t *py)
{
	struct gfx_txt_ci_data data;

	// Setup Data
	data.mode = GFX_TXT_CI_MODE_ITOC;
	data.idx = idx;
	data.found = 0;

	// Run through Text
	gfx_txt_run(dsp, x, y, w, h, skip_lines, font, s, l, &data, (gfx_txt_char_cb_t)gfx_txt_ci_char_cb);

	// Check Found
	if(data.found)
	{
		// Set Coordinates
		*px = data.px;
		*py = data.py;
		return 0;
	}

	return 1;
}

// Compute index for coordinates (returns 1 if not in area)
uint8_t gfx_txt_ctoi(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t px, uint16_t py, uint16_t *idx)
{
	struct gfx_txt_ci_data data;

	// Setup Data
	data.mode = GFX_TXT_CI_MODE_CTOI;
	data.px = px;
	data.py = py;
	data.found = 0;

	// Run through Text
	gfx_txt_run(dsp, x, y, w, h, skip_lines, font, s, l, &data, (gfx_txt_char_cb_t)gfx_txt_ci_char_cb);

	// Check Found
	if(data.found)
	{
		// Set Index
		*idx = data.idx;
		return 0;
	}

	return 1;
}

// Coordinates / Index Conversion Character Callback
void gfx_txt_ci_char_cb(struct gfx_txt_ci_data *data, struct gfx_dsp *dsp, int x, int y, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t idx, uint16_t px, uint16_t py, uint16_t line_idx)
{
	// Check Mode
	if(data->mode == GFX_TXT_CI_MODE_ITOC)
	{
		// Check Character Coordinates
		if((line_idx >= skip_lines) && (idx == data->idx))						{ data->px = px; data->py = py; data->found = 1; }
	}
	else if(data->mode == GFX_TXT_CI_MODE_CTOI)
	{
		// Check Character Coordinates
		if((line_idx >= skip_lines) && (px == data->px) && (py == data->py))	{ data->idx = idx; data->found = 1; }
	}
	else																		{ /* NoOp */ }
}

// Run through Text
void gfx_txt_run(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, void *user, gfx_txt_char_cb_t char_cb)
{
	uint16_t i;
	uint16_t j;
	uint16_t cw;
	uint16_t ch;
	uint16_t wlen;
	uint16_t lbreak;
	uint8_t c;
	uint16_t py;
	uint16_t skip;
	uint16_t ylimit;
	uint16_t line_idx;

	// Get Font Params
	cw = font->fw;
	ch = font->fh;

	// Adjust Parameters (Default to max width / single line)
	if(w == 0)																	{ w = gfx_dsp_get_width(dsp) - x; }
	if(h == 0)																	{ h = ch; }

	// Run through Text
	i = 0;
	py = 0;
	ylimit = y + h;
	line_idx = 0;
	while((i < l) && ((y + ((py + 1) * ch)) <= ylimit))
	{
		// Determine line length until last break char (space / nl)
		wlen = 0;
		skip = 0;
		lbreak = 0;
		while(((i + wlen) < l) && (wlen < (w / cw)) && (s[i + lbreak] != '\n'))
		{
			c = s[i + wlen];
			if((c == ' ') || (c == '\n'))										{ lbreak = wlen; }
			wlen = wlen + 1;
		}

		// Set Line Length (Break line if not last)
		if(((i + wlen) < l) && ((lbreak) || (s[i] == '\n')))					{ wlen = lbreak; skip = 1; }

		// Should we display yet? (skip lines)
		if(line_idx >= skip_lines)
		{
			// Draw Line
			for(j = 0; j < wlen; j = j + 1)										{ char_cb(user, dsp, x, y, skip_lines, font, s, l, i + j, j, py, line_idx); }

			// Inc Y
			py = py + 1;
		}

		// Loop
		line_idx = line_idx + 1;
		i = i + wlen + skip;
	}
}
