/* Dooba SDK
 * Generic Graphics Framework
 */

// External Includes
#include <string.h>

// Internal Includes
#include "gfx.h"
#include "img.h"

// Draw Full Image from RAM Memory
void gfx_img_full(struct gfx_dsp *dsp, uint8_t *img, int x, int y)
{
	// Draw Image
	gfx_img(dsp, img, x, y, 0, 0, *(uint16_t *)(gfx_img_width((uint16_t)img)), *(uint16_t *)(gfx_img_height((uint16_t)img)));
}

// Draw Image from RAM Memory
void gfx_img(struct gfx_dsp *dsp, uint8_t *img, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h)
{
	uint16_t img_w;
	uint16_t img_h;
	uint16_t i;
	uint16_t j;

	// Get Image Size
	img_w = *(uint16_t *)(gfx_img_width(img));
	img_h = *(uint16_t *)(gfx_img_height(img));

	// Clip Area
	if(gfx_dsp_clip(dsp, &x, &y, &img_x, &img_y, &w, &h))								{ return; }
	if((img_x + w) > img_w)																{ w = img_w - img_x; }
	if((img_y + h) > img_h)																{ h = img_h - img_y; }

	// Run through Image
	for(j = 0; j < h; j = j + 1)														{ for(i = 0; i < w; i = i + 1) { gfx_dsp_plot(dsp, x + i, y + j, *(uint16_t *)(gfx_img_data(img, (img_w * (img_y + j)) + img_x + i))); } }
}

// Draw Full Image from Program Memory
void gfx_img_p_full(struct gfx_dsp *dsp, uint32_t img, int x, int y)
{
	// Draw Image
	gfx_img_p(dsp, img, x, y, 0, 0, gfx_img_width_p(img), gfx_img_height_p(img));
}

// Draw Image from Program Memory
void gfx_img_p(struct gfx_dsp *dsp, uint32_t img, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h)
{
	uint16_t img_w;
	uint16_t img_h;
	uint16_t i;
	uint16_t j;
	uint16_t lw;
	uint16_t li;

	// Get Image Size
	img_w = gfx_img_width_p(img);
	img_h = gfx_img_height_p(img);

	// Clip Area
	if(gfx_dsp_clip(dsp, &x, &y, &img_x, &img_y, &w, &h))								{ return; }
	if((img_x + w) > img_w)																{ w = img_w - img_x; }
	if((img_y + h) > img_h)																{ h = img_h - img_y; }

	// Run through Image
	for(j = 0; j < h; j = j + 1)
	{
		i = 0;
		while(i < w)
		{
			// Determine Segment Size, Load it & Draw it
			lw = ((w - i) > GFX_DATA_BUF_SIZE) ? GFX_DATA_BUF_SIZE : (w - i);
			memcpy_PF(gfx_data_buf, gfx_img_data(img, (img_w * (img_y + j)) + img_x + i), lw * 2);
			for(li = 0; li < lw; li = li + 1)											{ gfx_dsp_plot(dsp, x + i + li, y + j, gfx_data_buf[li]); }
			i = i + lw;
		}
	}
}

// Draw Full Image from File System
void gfx_img_f_full(struct gfx_dsp *dsp, char *file, int x, int y)
{
	// Draw
	gfx_img_f_full_n(dsp, file, strlen(file), x, y);
}

// Draw Full Image from File System - Fixed Length
void gfx_img_f_full_n(struct gfx_dsp *dsp, char *file, uint8_t file_len, int x, int y)
{
	// Open Image
	if(vfs_open_n(&gfx_data_fp, file, file_len, 0))										{ return; }

	// Draw
	gfx_img_fp_full(dsp, &gfx_data_fp, x, y);

	// Close File
	vfs_close(&gfx_data_fp);
}

// Draw Image from File System
void gfx_img_f(struct gfx_dsp *dsp, char *file, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h)
{
	// Draw
	gfx_img_f_n(dsp, file, strlen(file), x, y, img_x, img_y, w, h);
}

// Draw Image from File System - Fixed Length
void gfx_img_f_n(struct gfx_dsp *dsp, char *file, uint8_t file_len, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h)
{
	// Clip Area
	if(gfx_dsp_clip(dsp, &x, &y, &img_x, &img_y, &w, &h))								{ return; }

	// Open Image
	if(vfs_open_n(&gfx_data_fp, file, file_len, 0))										{ return; }

	// Draw
	gfx_img_fp(dsp, &gfx_data_fp, x, y, img_x, img_y, w, h);

	// Close File
	vfs_close(&gfx_data_fp);
}

// Draw Full Image from File System (with file pointer)
void gfx_img_fp_full(struct gfx_dsp *dsp, struct vfs_handle *fp, int x, int y)
{
	uint16_t w;
	uint16_t h;

	// Read Image Size
	fp->pos = 0;
	if(vfs_read(fp, &w, 2, 0))															{ return; }
	if(vfs_read(fp, &h, 2, 0))															{ return; }

	// Draw Image
	gfx_img_fp(dsp, fp, x, y, 0, 0, w, h);
}

// Draw Image from File System (with file pointer)
void gfx_img_fp(struct gfx_dsp *dsp, struct vfs_handle *fp, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h)
{
	uint16_t img_w;
	uint16_t img_h;
	uint16_t i;
	uint16_t j;
	uint16_t lw;
	uint16_t li;

	// Read Image Size
	fp->pos = 0;
	if(vfs_read(fp, &img_w, 2, 0))														{ return; }
	if(vfs_read(fp, &img_h, 2, 0))														{ return; }

	// Clip Area
	if(gfx_dsp_clip(dsp, &x, &y, &img_x, &img_y, &w, &h))								{ return; }
	if((img_x + w) > img_w)																{ w = img_w - img_x; }
	if((img_y + h) > img_h)																{ h = img_h - img_y; }

	// Run through Image
	for(j = 0; j < h; j = j + 1)
	{
		i = 0;
		while(i < w)
		{
			// Determine Segment Size, Load it & Draw it
			lw = ((w - i) > GFX_DATA_BUF_SIZE) ? GFX_DATA_BUF_SIZE : (w - i);
			fp->pos = gfx_img_pix((img_w * (img_y + j)) + img_x + i);
			if(vfs_read(fp, gfx_data_buf, lw * 2, 0))									{ return; }
			for(li = 0; li < lw; li = li + 1)											{ gfx_dsp_plot(dsp, x + i + li, y + j, gfx_data_buf[li]); }
			i = i + lw;
		}
	}
}
