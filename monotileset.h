/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_MONOTILESET_H
#define	__GFX_MONOTILESET_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

// Internal Includes
#include "dsp.h"

// Default Frame Tiles
#define	GFX_MONOTILESET_FRAME_TL														0
#define	GFX_MONOTILESET_FRAME_TR														2
#define	GFX_MONOTILESET_FRAME_BL														6
#define	GFX_MONOTILESET_FRAME_BR														8
#define	GFX_MONOTILESET_FRAME_T															1
#define	GFX_MONOTILESET_FRAME_B															7
#define	GFX_MONOTILESET_FRAME_L															3
#define	GFX_MONOTILESET_FRAME_R															5
#define	GFX_MONOTILESET_FRAME_C															4

// Default Frame Tileset Arguments
#define	GFX_MONOTILESET_FRAME_ARGS														GFX_MONOTILESET_FRAME_TL, GFX_MONOTILESET_FRAME_TR, GFX_MONOTILESET_FRAME_BL, GFX_MONOTILESET_FRAME_BR, GFX_MONOTILESET_FRAME_T, GFX_MONOTILESET_FRAME_B, GFX_MONOTILESET_FRAME_L, GFX_MONOTILESET_FRAME_R, GFX_MONOTILESET_FRAME_C

// Tileset Dimensions
#define	gfx_monotileset_width(tileset)													(tileset)
#define	gfx_monotileset_height(tileset)													(tileset + 2)
#define	gfx_monotileset_twidth(tileset)													(tileset + 4)
#define	gfx_monotileset_theight(tileset)												(tileset + 5)

// Tileset Data Pointer
#define	gfx_monotileset_data(tileset, off)												(tileset + (off) + 6)

// Tileset Dimensions for Program Memory
#define	gfx_monotileset_width_p(tileset)												(pgm_read_word_far(gfx_monotileset_width(tileset)))
#define	gfx_monotileset_height_p(tileset)												(pgm_read_word_far(gfx_monotileset_height(tileset)))
#define	gfx_monotileset_twidth_p(tileset)												(pgm_read_byte_far(gfx_monotileset_twidth(tileset)))
#define	gfx_monotileset_theight_p(tileset)												(pgm_read_byte_far(gfx_monotileset_theight(tileset)))

// Tileset Data Pointer for Program Memory
#define	gfx_monotileset_data_p(tileset, off)											(pgm_read_byte_far(gfx_monotileset_data(tileset, off)))

// Determine Tile Count in Tileset
#define	gfx_monotileset_tiles(w, h, tw, th)												(((w) / (tw)) * ((h) / (th)))

// Shortcut - Draw Tiled Frame using default tile layout
#define gfx_monotileset_draw_frame_default(dsp, tileset, x, y, w, h)					gfx_monotileset_draw_frame(dsp, tileset, x, y, w, h, GFX_MONOTILESET_FRAME_ARGS)

// Tile Size (Bytes)
#define	gfx_monotileset_tile_size(tileset)												(((tileset)->tw * (tileset)->th) / 8)

// Monochrome Tileset Structure
struct gfx_monotileset
{
	// Tileset Data
	uint32_t data;

	// Tileset Size
	uint16_t w;
	uint16_t h;

	// Tile Size
	uint8_t tw;
	uint8_t th;

	// Tiles per Line
	uint16_t tpl;

	// Total Tiles
	uint16_t tiles;
};

// Initialize Monochrome Tileset
extern void gfx_monotileset_init(struct gfx_monotileset *tileset, uint32_t data);

// Draw Tile
extern void gfx_monotileset_draw(struct gfx_dsp *dsp, struct gfx_monotileset *tileset, uint16_t tile, int x, int y);

// Draw Partial Tile
extern void gfx_monotileset_draw_part(struct gfx_dsp *dsp, struct gfx_monotileset *tileset, uint16_t tile, int x, int y, uint16_t t_x, uint16_t t_y, uint16_t w, uint16_t h);

// Draw Tiled Frame
extern void gfx_monotileset_draw_frame(struct gfx_dsp *dsp, struct gfx_monotileset *tileset, int x, int y, uint16_t w, uint16_t h, uint16_t tl, uint16_t tr, uint16_t bl, uint16_t br, uint16_t t, uint16_t b, uint16_t l, uint16_t r, uint16_t c);

#endif
