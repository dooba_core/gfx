/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_COL_H
#define	__GFX_COL_H

// External Includes
#include <stdint.h>

// Generate Color
#define	gfx_col(r, g, b)						(((r) & 0xf8) << 8) | (((g) & 0xfc) << 3) | (((b) & 0xf8) >> 3)

// Generate Color from HTML code
#define	gfx_col_h(html)							gfx_col((((html) >> 16) & 0x00ff), (((html) >> 8) & 0x00ff), (((html) >> 0) & 0x00ff))

#endif
