/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_INVALIDATION_STACK_H
#define	__GFX_INVALIDATION_STACK_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Invalidation Stack Size
#ifndef	GFX_INVALIDATION_STACK_SIZE
#define	GFX_INVALIDATION_STACK_SIZE					8
#endif

// Invalidated Region Structure
struct gfx_invalidation_stack_reg
{
	// Region
	uint16_t x;
	uint16_t y;
	uint16_t w;
	uint16_t h;
};

// Invalidation Stack Structure
struct gfx_invalidation_stack
{
	// Invalidation Stack
	struct gfx_invalidation_stack_reg stack[GFX_INVALIDATION_STACK_SIZE];
	uint8_t stack_size;
};

// Has Invalid?
#define	gfx_invalidation_stack_has_invalid(s)											((s)->stack_size > 0)

// Initialize Invalidation Stack
extern void gfx_invalidation_stack_init(struct gfx_invalidation_stack *s);

// Invalidate Region
extern void gfx_invalidation_stack_invalidate(struct gfx_invalidation_stack *s, uint16_t dsp_w, uint16_t dsp_h, int x, int y, uint16_t w, uint16_t h);

// Pull Next Invalid Region from Invalidation Stack
extern void gfx_invalidation_stack_pull_invalid(struct gfx_invalidation_stack *s, uint32_t max_size, uint16_t *x, uint16_t *y, uint16_t *w, uint16_t *h);

#endif
