/* Dooba SDK
 * Generic Graphics Framework
 */

// External Includes
#include <string.h>
#include <stdlib.h>

// Internal Includes
#include "gfx.h"
#include "font.h"

// Initialize Font
void gfx_font_init(struct gfx_font *font, uint32_t data)
{
	// Get Font Info
	font->data = data;
	font->w = gfx_font_width_p(data);
	font->h = gfx_font_height_p(data);
	font->fw = gfx_font_fwidth_p(data);
	font->fh = gfx_font_fheight_p(data);
	font->cpl = font->w / font->fw;
	font->chars = gfx_font_chars(font->w, font->h, font->fw, font->fh);

	// Clear Cache
	memset(font->cache, 0, GFX_FONT_CACHE_SIZE);
}

// Acquire Character through Cache
uint8_t *gfx_font_get_char(struct gfx_font *font, uint8_t c)
{
	uint8_t *cache;
	uint16_t px;
	uint16_t py;
	uint8_t i;
	uint8_t j;
	uint16_t p;
	uint16_t k;
	uint16_t p_c;
	uint8_t c_off;
	uint8_t v;

	// Acquire Cache Char
	cache = gfx_font_cache_char(font, c);

	// Check Char
	if(cache[0] == c)											{ return &(cache[1]); }

	// Load Character
	cache[0] = c;
	v = 0;
	k = 0xffff;
	px = (c % font->cpl) * font->fw;
	py = (c / font->cpl) * font->fh;
	memset(&(cache[1]), 0, gfx_font_char_size(font));
	for(i = 0; i < font->fh; i = i + 1)
	{
		for(j = 0; j < font->fw; j = j + 1)
		{
			p_c = (i * font->fw) + j;
			c_off = 1 + (p_c / 8);
			p = ((py + i) * font->w) + px + j;
			if(k != (p / 8))									{ k = p / 8; v = gfx_font_data_p(font->data, k); }
			if((v >> (7 - (p % 8))) & 0x01)						{ cache[c_off] = cache[c_off] | (1 << (7 - (p_c % 8))); }
		}
	}

	return &(cache[1]);
}

// Draw Character
void gfx_font_draw(struct gfx_dsp *dsp, struct gfx_font *font, uint8_t c, int x, int y, uint16_t col)
{
	uint8_t i;
	uint8_t j;
	uint16_t px;
	uint16_t py;
	uint16_t p;
	uint16_t w;
	uint16_t h;
	uint8_t *cdata;

	// Check Character in Font
	if((c < 0x20) || ((c - 0x20) >= font->chars))				{ c = '.'; }
	c = c - 0x20;

	// Clear Font Coordinates
	px = 0;
	py = 0;
	w = font->fw;
	h = font->fh;

	// Clip Region
	if(gfx_dsp_clip(dsp, &x, &y, &px, &py, &w, &h))				{ return; }

	// Acquire Char through Cache & Draw
	cdata = gfx_font_get_char(font, c);
	if(cdata == 0)												{ return; }
	for(i = 0; i < h; i = i + 1)
	{
		for(j = 0; j < w; j = j + 1)
		{
			p = ((py + i) * font->fw) + px + j;
			if((cdata[p / 8] >> (7 - (p % 8))) & 0x01)			{ gfx_dsp_plot(dsp, x + j, y + i, col); }
		}
	}
}
