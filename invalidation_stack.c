/* Dooba SDK
 * Generic Graphics Framework
 */

// External Includes
#include <string.h>
#include <util/math.h>

// Internal Includes
#include "invalidation_stack.h"

// Initialize Invalidation Stack
void gfx_invalidation_stack_init(struct gfx_invalidation_stack *s)
{
	// Clear Stack
	s->stack_size = 0;
}

// Invalidate Region
void gfx_invalidation_stack_invalidate(struct gfx_invalidation_stack *s, uint16_t dsp_w, uint16_t dsp_h, int x, int y, uint16_t w, uint16_t h)
{
	uint8_t to_x;
	uint8_t to_y;
	struct gfx_invalidation_stack_reg *r;

	// Clip to Display
	if(clip_to_region(&x, &y, 0, 0, &w, &h, 0, 0, dsp_w, dsp_h))				{ return; }

	// Acquire Invalidation Stack Entry
	if(s->stack_size < GFX_INVALIDATION_STACK_SIZE)
	{
		// Simply push new entry onto stack
		r = &(s->stack[s->stack_size]);
		r->x = x;
		r->y = y;
		r->w = w;
		r->h = h;
		s->stack_size = s->stack_size + 1;
	}
	else
	{
		// Stack Overflow - Merge Last Entry
		r = &(s->stack[s->stack_size - 1]);

		// Save destination for currently invalidated region
		to_x = r->x + r->w;
		to_y = r->y + r->h;

		// Compute new region
		if(x < r->x)															{ r->x = x; }
		if(y < r->y)															{ r->y = y; }
		if((x + w) > to_x)														{ to_x = x + w; }
		if((y + h) > to_y)														{ to_y = y + h; }

		// Set Region
		r->w = to_x - r->x;
		r->h = to_y - r->y;
	}
}

// Pull Next Invalid Region from Invalidation Stack
void gfx_invalidation_stack_pull_invalid(struct gfx_invalidation_stack *s, uint32_t max_size, uint16_t *x, uint16_t *y, uint16_t *w, uint16_t *h)
{
	struct gfx_invalidation_stack_reg *r;
	uint16_t rh;
	uint8_t i;

	// Clear Invalidated Region
	*w = 0;
	*h = 0;

	// Check Stack
	if(s->stack_size == 0)														{ return; }

	// Load First Invalid Region
	r = &(s->stack[0]);

	// Acquire Region Height & Limit
	rh = r->h;
	if((max_size > 0) && (rh > (max_size / r->w)))								{ rh = max_size / r->w; }

	// Set Currently Invalidated Region
	*x = r->x;
	*y = r->y;
	*w = r->w;
	*h = rh;

	// Cut Region
	r->y = r->y + rh;
	r->h = r->h - rh;
	if(r->h == 0)
	{
		// Drop from Stack
		s->stack_size = s->stack_size - 1;
		for(i = 0; i < s->stack_size; i = i + 1)								{ memcpy(&(s->stack[i]), &(s->stack[i + 1]), sizeof(struct gfx_invalidation_stack_reg)); }
	}
}
