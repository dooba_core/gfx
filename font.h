/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_FONT_H
#define	__GFX_FONT_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

// Internal Includes
#include "dsp.h"

// Font Data Cache Size
#ifndef	GFX_FONT_CACHE_SIZE
#define	GFX_FONT_CACHE_SIZE							64
#endif

// Font Dimensions
#define	gfx_font_width(font)						(font)
#define	gfx_font_height(font)						(font + 2)
#define	gfx_font_fwidth(font)						(font + 4)
#define	gfx_font_fheight(font)						(font + 5)

// Font Data Pointer
#define	gfx_font_data(font, off)					(font + (off) + 6)

// Font Dimensions for Program Memory
#define	gfx_font_width_p(font)						(pgm_read_word_far(gfx_font_width(font)))
#define	gfx_font_height_p(font)						(pgm_read_word_far(gfx_font_height(font)))
#define	gfx_font_fwidth_p(font)						(pgm_read_byte_far(gfx_font_fwidth(font)))
#define	gfx_font_fheight_p(font)					(pgm_read_byte_far(gfx_font_fheight(font)))

// Font Data Pointer for Program Memory
#define	gfx_font_data_p(font, off)					(pgm_read_byte_far(gfx_font_data(font, off)))

// Determine Character Count in Font
#define	gfx_font_chars(w, h, fw, fh)				(((w) * (h)) / ((fw) * (fh)))

// Character Size (Bytes)
#define	gfx_font_char_size(font)					(((font)->fw * (font)->fh) / 8)

// Character Size in Cache (Bytes)
#define	gfx_font_cache_char_size(font)				(1 + gfx_font_char_size(font))

// Cache Size (Entries)
#define	gfx_font_cache_size(font)					(GFX_FONT_CACHE_SIZE / gfx_font_cache_char_size(font))

// Cache Char
#define	gfx_font_cache_char(font, c)				(&((font)->cache[((c) % gfx_font_cache_size(font)) * gfx_font_cache_char_size(font)]))

// Font Structure
struct gfx_font
{
	// Font Data
	uint32_t data;

	// Data Cache
	uint8_t cache[GFX_FONT_CACHE_SIZE];

	// Font Size
	uint16_t w;
	uint16_t h;

	// Char Size
	uint8_t fw;
	uint8_t fh;

	// Chars per Line
	uint8_t cpl;

	// Total Chars
	uint8_t chars;
};

// Initialize Font
extern void gfx_font_init(struct gfx_font *font, uint32_t data);

// Acquire Character through Cache
extern uint8_t *gfx_font_get_char(struct gfx_font *font, uint8_t c);

// Draw Character
extern void gfx_font_draw(struct gfx_dsp *dsp, struct gfx_font *font, uint8_t c, int x, int y, uint16_t col);

#endif
