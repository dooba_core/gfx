/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_DSP_H
#define	__GFX_DSP_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <vfs/vfs.h>

// Internal Includes
#include "invalidation_stack.h"

// Display Orientation
#define	GFX_DSP_ORIENTATION_TOP							0x00
#define	GFX_DSP_ORIENTATION_RIGHT						0x01
#define	GFX_DSP_ORIENTATION_BOTTOM						0x02
#define	GFX_DSP_ORIENTATION_LEFT						0x03

// Display Driver Callback Types
typedef	void (*gfx_dsp_set_orientation_t)(void *dsp, void *user, uint8_t orientation);
typedef	uint16_t (*gfx_dsp_get_width_t)(void *dsp, void *user);
typedef	uint16_t (*gfx_dsp_get_height_t)(void *dsp, void *user);
typedef	void (*gfx_dsp_on_t)(void *dsp, void *user);
typedef	void (*gfx_dsp_off_t)(void *dsp, void *user);
typedef	void (*gfx_dsp_plot_t)(void *dsp, void *user, uint16_t x, uint16_t y, uint16_t c);
typedef	void (*gfx_dsp_clr_t)(void *dsp, void *user, int x, int y, uint16_t w, uint16_t h);
typedef	void (*gfx_dsp_commit_t)(void *dsp, void *user);

// Display Structure
struct gfx_dsp
{
	// User Data
	void *user;

	// Display Orientation
	uint8_t orientation;

	// Invalidation Stack
	struct gfx_invalidation_stack inv_stack;

	// Maximum Invalid Region Size (in pixels) (will default to entire display)
	uint32_t max_inv_size;

	// Current Invalidated Region
	uint16_t inv_x;
	uint16_t inv_y;
	uint16_t inv_w;
	uint16_t inv_h;

	// Direct Drawing
	uint8_t direct_draw;

	// Set Display Orientation
	gfx_dsp_set_orientation_t set_orientation;

	// Get Display Width
	gfx_dsp_get_width_t get_width;

	// Get Display Height
	gfx_dsp_get_height_t get_height;

	// Display On
	gfx_dsp_on_t dsp_on;

	// Display Off
	gfx_dsp_off_t dsp_off;

	// Plot Pixel
	gfx_dsp_plot_t plot;

	// Clear Area
	gfx_dsp_clr_t clr;

	// Commit Data to Display
	gfx_dsp_commit_t commit;
};

// Initialize Display
extern void gfx_dsp_init(struct gfx_dsp *dsp, void *user);

// Set Maximum Invalid Region Size
extern void gfx_dsp_set_max_inv_size(struct gfx_dsp *dsp, uint32_t size);

// Set Display Orientation
extern void gfx_dsp_set_orientation(struct gfx_dsp *dsp, uint8_t orientation);

// Get Display Orientation
extern uint8_t gfx_dsp_get_orientation(struct gfx_dsp *dsp);

// Get Display Width
extern uint16_t gfx_dsp_get_width(struct gfx_dsp *dsp);

// Get Display Height
extern uint16_t gfx_dsp_get_height(struct gfx_dsp *dsp);

// Display On
extern void gfx_dsp_on(struct gfx_dsp *dsp);

// Display Off
extern void gfx_dsp_off(struct gfx_dsp *dsp);

// Invalidate
extern void gfx_dsp_invalidate(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h);

// Start Drawing Invalidated Frame (returns 1 if there's nothing to draw)
extern uint8_t gfx_dsp_start_inv(struct gfx_dsp *dsp);

// Complete Drawing Invalidated Frame
extern void gfx_dsp_stop_inv(struct gfx_dsp *dsp);

// Start Direct Drawing
extern void gfx_dsp_start(struct gfx_dsp *dsp);

// Stop Direct Drawing
extern void gfx_dsp_stop(struct gfx_dsp *dsp);

// Flush Data to Display
extern void gfx_dsp_flush(struct gfx_dsp *dsp);

// Perform Clipping (returns 1 if there's nothing to draw)
extern uint8_t gfx_dsp_clip(struct gfx_dsp *dsp, int *x, int *y, uint16_t *px, uint16_t *py, uint16_t *w, uint16_t *h);

// Plot Pixel
extern void gfx_dsp_plot(struct gfx_dsp *dsp, uint16_t x, uint16_t y, uint16_t c);

// Clear Area
extern void gfx_dsp_clr(struct gfx_dsp *dsp, uint16_t x, uint16_t y, uint16_t w, uint16_t h);

// Clear All
extern void gfx_dsp_clr_all(struct gfx_dsp *dsp);

#endif
