/* Dooba SDK
 * Generic Graphics Framework
 */

// External Includes
#include <string.h>
#include <util/math.h>

// Internal Includes
#include "gfx.h"
#include "img.h"
#include "dsp.h"

// Initialize Display
void gfx_dsp_init(struct gfx_dsp *dsp, void *user)
{
	// Clear all implementation details
	memset(dsp, 0, sizeof(struct gfx_dsp));

	// Set User
	dsp->user = user;

	// Clear Current Invalid Region
	dsp->inv_x = 0;
	dsp->inv_y = 0;
	dsp->inv_w = 0;
	dsp->inv_h = 0;

	// Disable Direct Drawing
	dsp->direct_draw = 0;

	// Clear Maximum Invalid Size
	dsp->max_inv_size = 0;

	// Initialize Invalidation Stack
	gfx_invalidation_stack_init(&(dsp->inv_stack));

	// Set Orientation
	dsp->orientation = GFX_DSP_ORIENTATION_TOP;
}

// Set Maximum Invalid Region Size
void gfx_dsp_set_max_inv_size(struct gfx_dsp *dsp, uint32_t size)
{
	// Set Maximum Invalid Region Size
	dsp->max_inv_size = size;
}

// Set Display Orientation
void gfx_dsp_set_orientation(struct gfx_dsp *dsp, uint8_t orientation)
{
	// Set Orientation
	if(dsp->set_orientation == 0)																				{ return; }
	dsp->set_orientation(dsp, dsp->user, orientation);

	// Set Orientation
	dsp->orientation = orientation;
}

// Get Display Orientation
uint8_t gfx_dsp_get_orientation(struct gfx_dsp *dsp)
{
	// Orientation
	return dsp->orientation;
}

// Get Display Width
uint16_t gfx_dsp_get_width(struct gfx_dsp *dsp)
{
	// Get Width
	if(dsp->get_width == 0)																						{ return 0; }
	return dsp->get_width(dsp, dsp->user);
}

// Get Display Height
uint16_t gfx_dsp_get_height(struct gfx_dsp *dsp)
{
	// Get Height
	if(dsp->get_height == 0)																					{ return 0; }
	return dsp->get_height(dsp, dsp->user);
}

// Display On
void gfx_dsp_on(struct gfx_dsp *dsp)
{
	// Display On
	if(dsp->dsp_on == 0)																						{ return; }
	dsp->dsp_on(dsp, dsp->user);
}

// Display Off
void gfx_dsp_off(struct gfx_dsp *dsp)
{
	// Display Off
	if(dsp->dsp_off == 0)																						{ return; }
	dsp->dsp_off(dsp, dsp->user);
}

// Invalidate
void gfx_dsp_invalidate(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h)
{
	// Invalidate
	gfx_invalidation_stack_invalidate(&(dsp->inv_stack), gfx_dsp_get_width(dsp), gfx_dsp_get_height(dsp), x, y, w, h);
}

// Start Drawing Frame
uint8_t gfx_dsp_start_inv(struct gfx_dsp *dsp)
{
	// Pull Next Invalid Region
	gfx_invalidation_stack_pull_invalid(&(dsp->inv_stack), dsp->max_inv_size, &(dsp->inv_x), &(dsp->inv_y), &(dsp->inv_w), &(dsp->inv_h));

	// Check if there is something to draw
	return (dsp->inv_w == 0) || (dsp->inv_h == 0);
}

// Complete Drawing Frame
void gfx_dsp_stop_inv(struct gfx_dsp *dsp)
{
	// Flush
	gfx_dsp_flush(dsp);
}

// Start Direct Drawing
void gfx_dsp_start(struct gfx_dsp *dsp)
{
	// Start Direct Drawing
	dsp->direct_draw = 1;
	dsp->inv_x = 0;
	dsp->inv_y = 0;
	dsp->inv_w = 0;
	dsp->inv_h = 0;
}

// Stop Direct Drawing
void gfx_dsp_stop(struct gfx_dsp *dsp)
{
	// Flush
	gfx_dsp_flush(dsp);

	// Stop Direct Drawing
	dsp->direct_draw = 0;
}

// Flush Data to Display
void gfx_dsp_flush(struct gfx_dsp *dsp)
{
	// Flush
	if(dsp->commit)																								{ dsp->commit(dsp, dsp->user); }
	dsp->inv_w = 0;
	dsp->inv_h = 0;
}

// Perform Clipping
uint8_t gfx_dsp_clip(struct gfx_dsp *dsp, int *x, int *y, uint16_t *px, uint16_t *py, uint16_t *w, uint16_t *h)
{
	// Clip to Display
	if(clip_to_region(x, y, px, py, w, h, 0, 0, gfx_dsp_get_width(dsp), gfx_dsp_get_height(dsp)))				{ return 1; }

	// Check Direct Drawing
	if(dsp->direct_draw)																						{ return 0; }

	// Clip to Invalidated Region
	if(clip_to_region(x, y, px, py, w, h, dsp->inv_x, dsp->inv_y, dsp->inv_w, dsp->inv_h))						{ return 1; }

	return 0;
}

// Plot Pixel
void gfx_dsp_plot(struct gfx_dsp *dsp, uint16_t x, uint16_t y, uint16_t c)
{
	// Check Pixel Still Valid
	if((dsp->direct_draw == 0) && (gfx_is_point_still_valid(dsp, x, y)))										{ return; }

	// Plot Pixel
	if(dsp->plot == 0)																							{ return; }
	dsp->plot(dsp, dsp->user, x, y, c);
}

// Clear Area
void gfx_dsp_clr(struct gfx_dsp *dsp, uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
	int xi;
	int yi;

	// Clip Area
	xi = x;
	yi = y;
	if(gfx_dsp_clip(dsp, &xi, &yi, 0, 0, &w, &h))																{ return; }

	// Clear using overridden implementation if provided
	if(dsp->clr)																								{ dsp->clr(dsp, dsp->user, xi, yi, w, h); return; }

	/* Generic Implementation
	 */

	// Fill Area with Black
	gfx_draw_fill(dsp, xi, yi, w, h, 0);
}

// Clear All
void gfx_dsp_clr_all(struct gfx_dsp *dsp)
{
	// Clear
	gfx_dsp_clr(dsp, 0, 0, gfx_dsp_get_width(dsp), gfx_dsp_get_height(dsp));
}
