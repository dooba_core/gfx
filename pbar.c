/* Dooba SDK
 * Generic Graphics Framework
 */

// External Includes
#include <string.h>

// Internal Includes
#include "gfx.h"
#include "pbar.h"

// Draw Progress Bar
void gfx_pbar_draw(struct gfx_dsp *dsp, struct gfx_tileset *tlst, uint8_t pct, uint16_t w, int x, int y, uint16_t l, uint16_t c, uint16_t r, uint16_t fl, uint16_t fc, uint16_t fr)
{
	uint16_t i;
	uint16_t tw;
	uint16_t th;
	uint16_t len;
	uint16_t midlen;
	uint16_t plen;

	// Compute Length (of active part)
	if(pct > 100)									{ pct = 100; }
	tw = tlst->tw;
	th = tlst->th;
	w = w - (w % tw);
	if(w <= (tw * 2))								{ return; }
	len = w * pct;
	if(len > 0)										{ len = len / 100; }

	// Draw Background (Empty)
	gfx_tileset_draw(dsp, tlst, l, x, y);			// Start
	i = 0;
	midlen = w - (tw * 2);
	while(i < midlen)
	{
		// Draw mid part
		gfx_tileset_draw(dsp, tlst, c, x + tw + i, y);

		// Next
		i = i + tw;
	}
	gfx_tileset_draw(dsp, tlst, r, x + tw + i, y);	// End

	// Fill
	i = 0;
	while(i < len)
	{
		// Determine part len
		plen = ((len - i) > tw) ? tw : (len - i);

		// Draw part
		if(i == 0)									{ gfx_tileset_draw_part(dsp, tlst, fl, x, y, 0, 0, plen, th); }
		else
		{
			if(i >= (tw + midlen))					{ gfx_tileset_draw_part(dsp, tlst, fr, x + i, y, 0, 0, plen, th); }
			else									{ gfx_tileset_draw_part(dsp, tlst, fc, x + i, y, 0, 0, plen, th); }
		}

		// Next
		i = i + plen;
	}
}
