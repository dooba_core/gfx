/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_H
#define	__GFX_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <vfs/vfs.h>

// Internal Includes
#include "dsp.h"
#include "col.h"
#include "txt.h"
#include "img.h"
#include "font.h"
#include "pbar.h"
#include "tileset.h"

// Data Buffer Size
#define	GFX_DATA_BUF_SIZE							32

// Point Validation Checks
#define	gfx_is_point_invalidated(dsp, x, y)			(((x) >= (dsp)->inv_x) && ((x) < ((dsp)->inv_x + (dsp)->inv_w)) && ((y) >= (dsp)->inv_y) && ((y) < ((dsp)->inv_y + (dsp)->inv_h)))
#define	gfx_is_point_still_valid(dsp, x, y)			(((x) < (dsp)->inv_x) || ((x) >= ((dsp)->inv_x + (dsp)->inv_w)) || ((y) < (dsp)->inv_y) || ((y) >= ((dsp)->inv_y + (dsp)->inv_h)))

// Data Buffer
extern uint16_t gfx_data_buf[GFX_DATA_BUF_SIZE];

// File Pointer
extern struct vfs_handle gfx_data_fp;

// Draw Line
extern void gfx_draw_line(struct gfx_dsp *dsp, int from_x, int from_y, int to_x, int to_y, uint16_t c);

// Draw Fill
extern void gfx_draw_fill(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t c);

// Draw Rectangle
extern void gfx_draw_rect(struct gfx_dsp *dsp, int x, int y, int w, int h, uint16_t c);

// Draw N-Gon (n = number of lines, varargs = x0, y0, x1, y1, x2, y2)
extern void gfx_draw_ngon(struct gfx_dsp *dsp, uint16_t c, uint8_t n, ...);

// Draw N-Gon (variable argument list)
extern void gfx_draw_ngon_v(struct gfx_dsp *dsp, uint16_t c, uint8_t n, va_list ap);

// Draw Circle
extern void gfx_draw_circle(struct gfx_dsp *dsp, int x, int y, uint16_t r, uint16_t c);

#endif
