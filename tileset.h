/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_TILESET_H
#define	__GFX_TILESET_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

// Internal Includes
#include "dsp.h"

// Palette Size
#define	GFX_TILESET_PAL_SIZE														255

// Default Frame Tiles
#define	GFX_TILESET_FRAME_TL														0
#define	GFX_TILESET_FRAME_TR														2
#define	GFX_TILESET_FRAME_BL														6
#define	GFX_TILESET_FRAME_BR														8
#define	GFX_TILESET_FRAME_T															1
#define	GFX_TILESET_FRAME_B															7
#define	GFX_TILESET_FRAME_L															3
#define	GFX_TILESET_FRAME_R															5
#define	GFX_TILESET_FRAME_C															4

// Default Frame Tileset Arguments
#define	GFX_TILESET_FRAME_ARGS														GFX_TILESET_FRAME_TL, GFX_TILESET_FRAME_TR, GFX_TILESET_FRAME_BL, GFX_TILESET_FRAME_BR, GFX_TILESET_FRAME_T, GFX_TILESET_FRAME_B, GFX_TILESET_FRAME_L, GFX_TILESET_FRAME_R, GFX_TILESET_FRAME_C

// Tileset Dimensions
#define	gfx_tileset_width(tileset)													(tileset)
#define	gfx_tileset_height(tileset)													(tileset + 2)
#define	gfx_tileset_twidth(tileset)													(tileset + 4)
#define	gfx_tileset_theight(tileset)												(tileset + 5)

// Tileset Palette
#define	gfx_tileset_pal_col(tileset, col)											(tileset + (((col) + 3) * 2))

// Tileset Data Pointer
#define	gfx_tileset_data(tileset, off)												(tileset + (off) + 6 + (GFX_TILESET_PAL_SIZE * sizeof(uint16_t)))

// Tileset Dimensions for Program Memory
#define	gfx_tileset_width_p(tileset)												(pgm_read_word_far(gfx_tileset_width(tileset)))
#define	gfx_tileset_height_p(tileset)												(pgm_read_word_far(gfx_tileset_height(tileset)))
#define	gfx_tileset_twidth_p(tileset)												(pgm_read_byte_far(gfx_tileset_twidth(tileset)))
#define	gfx_tileset_theight_p(tileset)												(pgm_read_byte_far(gfx_tileset_theight(tileset)))

// Tileset Palette Color for Program Memory
#define	gfx_tileset_pal_col_p(tileset, col)											(pgm_read_word_far(gfx_tileset_pal_col(tileset, col)))

// Tileset Data Pointer for Program Memory
#define	gfx_tileset_data_p(tileset, off)											(pgm_read_byte_far(gfx_tileset_data(tileset, off)))

// Determine Tile Count in Tileset
#define	gfx_tileset_tiles(w, h, tw, th)												(((w) / (tw)) * ((h) / (th)))

// Shortcut - Draw Tiled Frame using default tile layout
#define gfx_tileset_draw_frame_default(dsp, tileset, x, y, w, h)					gfx_tileset_draw_frame(dsp, tileset, x, y, w, h, GFX_TILESET_FRAME_ARGS)

// Tile Size (Bytes)
#define	gfx_tileset_tile_size(tileset)												((tileset)->tw * (tileset)->th)

// Tileset Structure
struct gfx_tileset
{
	// Tileset Data
	uint32_t data;

	// Tileset Size
	uint16_t w;
	uint16_t h;

	// Tile Size
	uint8_t tw;
	uint8_t th;

	// Tiles per Line
	uint16_t tpl;

	// Total Tiles
	uint16_t tiles;
};

// Initialize Tileset from full address
extern void gfx_tileset_init(struct gfx_tileset *tileset, uint32_t data);

// Draw Tile
extern void gfx_tileset_draw(struct gfx_dsp *dsp, struct gfx_tileset *tileset, uint16_t tile, int x, int y);

// Draw Partial Tile
extern void gfx_tileset_draw_part(struct gfx_dsp *dsp, struct gfx_tileset *tileset, uint16_t tile, int x, int y, uint16_t t_x, uint16_t t_y, uint16_t w, uint16_t h);

// Draw Tiled Frame
extern void gfx_tileset_draw_frame(struct gfx_dsp *dsp, struct gfx_tileset *tileset, int x, int y, uint16_t w, uint16_t h, uint16_t tl, uint16_t tr, uint16_t bl, uint16_t br, uint16_t t, uint16_t b, uint16_t l, uint16_t r, uint16_t c);

#endif
