/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_TXT_H
#define	__GFX_TXT_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

// Internal Includes
#include "font.h"
#include "dsp.h"

// Text Buffer Size
#ifndef	GFX_TXT_BUFSIZE
#define	GFX_TXT_BUFSIZE											256
#endif

// Coordinates / Index Conversion Mode
#define	GFX_TXT_CI_MODE_ITOC									0
#define	GFX_TXT_CI_MODE_CTOI									1

// Shortcuts
#define	gfx_txt(dsp, s, x, y, w, h, font, col)					gfx_txt_s(dsp, s, x, y, w, h, 0, font, col)
#define	gfx_txt_n(dsp, s, l, x, y, w, h, font, col)				gfx_txt_s_n(dsp, s, l, x, y, w, h, 0, font, col)
#define	gfx_txt_f(dsp, x, y, w, h, font, col, fmt, ...)			gfx_txt_s_f(dsp, x, y, w, h, 0, font, col, fmt, ##__VA_ARGS__)
#define	gfx_txt_v(dsp, x, y, w, h, font, col, fmt, ap)			gfx_txt_s_v(dsp, x, y, w, h, 0, font, col, fmt, ap)
#define	gfx_txt_vp(dsp, x, y, w, h, font, col, fmt, ap)			gfx_txt_s_vp(dsp, x, y, w, h, 0, font, col, fmt, ap)

// Character Callback Type
typedef	void (*gfx_txt_char_cb_t)(void *data, struct gfx_dsp *dsp, int x, int y, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t idx, uint16_t px, uint16_t py, uint16_t line_idx);

// Text Buffer
extern char gfx_txt_buf[GFX_TXT_BUFSIZE];

// Draw Text Data Structure
struct gfx_txt_data
{
	// Color
	uint16_t col;
};

// Coordinates / Index Data Structure
struct gfx_txt_ci_data
{
	// Looking for what?
	uint8_t mode;

	// Found
	uint8_t found;

	// Index
	uint16_t idx;

	// Coordinates
	uint16_t px;
	uint16_t py;
};

// Draw Text (Skip Lines)
extern void gfx_txt_s(struct gfx_dsp *dsp, void *s, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col);

// Draw Text (Skip Lines) - Safe
extern void gfx_txt_s_n(struct gfx_dsp *dsp, void *s, uint16_t l, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col);

// Draw Text (Skip Lines) - Format String
extern void gfx_txt_s_f(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col, void *fmt, ...);

// Draw Text (Skip Lines) - va_list
extern void gfx_txt_s_v(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col, void *fmt, va_list ap);

// Draw Text (Skip Lines) - va_list*
extern void gfx_txt_s_vp(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, uint16_t col, void *fmt, va_list *ap);

// Draw Text Character Callback
extern void gfx_txt_char_cb(struct gfx_txt_data *data, struct gfx_dsp *dsp, int x, int y, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t idx, uint16_t px, uint16_t py, uint16_t line_idx);

// Compute coordinates for index (returns 1 if not in area)
extern uint8_t gfx_txt_itoc(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t idx, uint16_t *px, uint16_t *py);

// Compute index for coordinates (returns 1 if not in area)
extern uint8_t gfx_txt_ctoi(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t px, uint16_t py, uint16_t *idx);

// Coordinates / Index Conversion Character Callback
extern void gfx_txt_ci_char_cb(struct gfx_txt_ci_data *data, struct gfx_dsp *dsp, int x, int y, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, uint16_t idx, uint16_t px, uint16_t py, uint16_t line_idx);

// Run through Text
extern void gfx_txt_run(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t skip_lines, struct gfx_font *font, char *s, uint16_t l, void *user, gfx_txt_char_cb_t char_cb);

#endif
