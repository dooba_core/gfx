/* Dooba SDK
 * Generic Graphics Framework
 */

// External Includes
#include <string.h>

// Internal Includes
#include "gfx.h"
#include "monotileset.h"

// Initialize Monochrome Tileset
void gfx_monotileset_init(struct gfx_monotileset *tileset, uint32_t data)
{
	// Get Font Info
	tileset->data = data;
	tileset->w = gfx_monotileset_width_p(data);
	tileset->h = gfx_monotileset_height_p(data);
	tileset->tw = gfx_monotileset_twidth_p(data);
	tileset->th = gfx_monotileset_theight_p(data);
	tileset->tpl = tileset->w / tileset->tw;
	tileset->tiles = gfx_monotileset_tiles(tileset->w, tileset->h, tileset->tw, tileset->th);
}

// Draw Tile
void gfx_monotileset_draw(struct gfx_dsp *dsp, struct gfx_monotileset *tileset, uint16_t tile, int x, int y)
{
	// Draw Complete Tile
	gfx_monotileset_draw_part(dsp, tileset, tile, x, y, 0, 0, tileset->tw, tileset->th);
}

// Draw Partial Tile
void gfx_monotileset_draw_part(struct gfx_dsp *dsp, struct gfx_monotileset *tileset, uint16_t tile, int x, int y, uint16_t t_x, uint16_t t_y, uint16_t w, uint16_t h)
{
	uint8_t i;
	uint8_t j;
	uint8_t v;
	uint16_t px;
	uint16_t py;
	uint16_t p;
	uint16_t k;

	// Check Tile in Set
	if(tile >= tileset->tiles)								{ return; }

	// Clip Region
	if((t_x >= tileset->tw) || (t_y >= tileset->th))		{ return; }
	if(gfx_dsp_clip(dsp, &x, &y, &t_x, &t_y, &w, &h))		{ return; }
	if((t_x + w) > tileset->tw)								{ w = tileset->tw - t_x; }
	if((t_y + h) > tileset->th)								{ h = tileset->th - t_y; }

	// Draw Tile
	px = (tile % tileset->tpl) * tileset->tw;
	py = (tile / tileset->tpl) * tileset->th;

	v = 0;
	k = 0xffff;
	for(i = 0; i < h; i = i + 1)
	{
		for(j = 0; j < w; j = j + 1)
		{
			p = ((py + t_y + i) * tileset->w) + px + t_x + j;
			if(k != (p / 8))									{ k = p / 8; v = gfx_monotileset_data_p(tileset->data, k); }
			if((v >> (7 - (p % 8))) & 0x01)						{ gfx_dsp_plot(dsp, x + j, y + i, 0xffff); }
		}
	}
}

// Draw Tiled Frame
void gfx_monotileset_draw_frame(struct gfx_dsp *dsp, struct gfx_monotileset *tileset, int x, int y, uint16_t w, uint16_t h, uint16_t tl, uint16_t tr, uint16_t bl, uint16_t br, uint16_t t, uint16_t b, uint16_t l, uint16_t r, uint16_t c)
{
	uint8_t i;
	uint8_t j;
	uint8_t tw;
	uint8_t th;
	uint8_t sw;
	uint8_t sh;

	// Acquire Shortcuts
	tw = tileset->tw;
	th = tileset->th;
	sw = w - tw;
	sh = h - th;

	// Run from Top to Bottom & Left to Right
	for(i = 0; i <= sh; i = i + th)
	{
		for(j = 0; j <= sw; j = j + tw)
		{
			if(i == 0)
			{
				if(j == 0)									{ gfx_monotileset_draw(dsp, tileset, tl, x + j, y + i); }
				else if((j + tw) > sw)						{ gfx_monotileset_draw_part(dsp, tileset, t, x + j, y + i, 0, 0, sw - j, tileset->th); gfx_monotileset_draw(dsp, tileset, tr, x + sw, y + i); }
				else										{ gfx_monotileset_draw(dsp, tileset, t, x + j, y + i); }
			}
			else if((i + th) > sh)
			{
				if(j == 0)									{ gfx_monotileset_draw_part(dsp, tileset, l, x + j, y + i, 0, 0, tileset->tw, sh - i); gfx_monotileset_draw(dsp, tileset, bl, x + j, y + sh); }
				else if((j + tw) > sw)						{ gfx_monotileset_draw_part(dsp, tileset, c, x + j, y + i, 0, 0, sw - j, sh - i); gfx_monotileset_draw_part(dsp, tileset, r, x + sw, y + i, 0, 0, tileset->tw, sh - i); gfx_monotileset_draw_part(dsp, tileset, b, x + j, y + sh, 0, 0, sw - j, tileset->th); gfx_monotileset_draw(dsp, tileset, br, x + sw, y + sh); }
				else										{ gfx_monotileset_draw_part(dsp, tileset, c, x + j, y + i, 0, 0, tileset->tw, sh - i); gfx_monotileset_draw(dsp, tileset, b, x + j, y + sh); }
			}
			else
			{
				if(j == 0)									{ gfx_monotileset_draw(dsp, tileset, l, x + j, y + i); }
				else if((j + tw) > sw)						{ gfx_monotileset_draw_part(dsp, tileset, c, x + j, y + i, 0, 0, sw - j, tileset->tw); gfx_monotileset_draw(dsp, tileset, r, x + sw, y + i); }
				else										{ gfx_monotileset_draw(dsp, tileset, c, x + j, y + i); }
			}
		}
	}
}
