/* Dooba SDK
 * Generic Graphics Framework
 */

#ifndef	__GFX_IMG_H
#define	__GFX_IMG_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <vfs/vfs.h>

// Internal Includes
#include "dsp.h"

// Image Dimensions
#define	gfx_img_width(img)								(img)
#define	gfx_img_height(img)								(img + 2)

// Image Pixel Offset
#define	gfx_img_pix(idx)								((idx * 2) + 4)

// Image Pixel
#define	gfx_img_data(img, idx)							(img + gfx_img_pix(idx))

// Image Dimensions for Program Memory
#define	gfx_img_width_p(img)							(pgm_read_byte_far(gfx_img_width(img)))
#define	gfx_img_height_p(img)							(pgm_read_byte_far(gfx_img_height(img)))

// Image Pixel in Program Memory
#define	gfx_img_data_p(img, idx)						(pgm_read_byte_far(gfx_img_data(img, idx)))

// Draw Full Image from RAM Memory
extern void gfx_img_full(struct gfx_dsp *dsp, uint8_t *img, int x, int y);

// Draw Image from RAM Memory
extern void gfx_img(struct gfx_dsp *dsp, uint8_t *img, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h);

// Draw Full Image from Program Memory
extern void gfx_img_p_full(struct gfx_dsp *dsp, uint32_t img, int x, int y);

// Draw Image from Program Memory
extern void gfx_img_p(struct gfx_dsp *dsp, uint32_t img, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h);

// Draw Full Image from File System
extern void gfx_img_f_full(struct gfx_dsp *dsp, char *file, int x, int y);

// Draw Full Image from File System - Fixed Length
extern void gfx_img_f_full_n(struct gfx_dsp *dsp, char *file, uint8_t file_len, int x, int y);

// Draw Image from File System
extern void gfx_img_f(struct gfx_dsp *dsp, char *file, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h);

// Draw Image from File System - Fixed Length
extern void gfx_img_f_n(struct gfx_dsp *dsp, char *file, uint8_t file_len, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h);

// Draw Full Image from File System (with file pointer)
extern void gfx_img_fp_full(struct gfx_dsp *dsp, struct vfs_handle *fp, int x, int y);

// Draw Image from File System (with file pointer)
extern void gfx_img_fp(struct gfx_dsp *dsp, struct vfs_handle *fp, int x, int y, uint16_t img_x, uint16_t img_y, uint16_t w, uint16_t h);

#endif
