/* Dooba SDK
 * Generic Graphics Framework
 */

// External Includes
#include <stdlib.h>
#include <string.h>

// Internal Includes
#include "gfx.h"

// Data Buffer
uint16_t gfx_data_buf[GFX_DATA_BUF_SIZE];

// File Pointer
struct vfs_handle gfx_data_fp;

// Draw Line
void gfx_draw_line(struct gfx_dsp *dsp, int from_x, int from_y, int to_x, int to_y, uint16_t c)
{
	int dx;
	int dy;
	int sx;
	int sy;
	int err;
	int e2;

	// Compute Deltas
	dx = abs(to_x - from_x);
	dy = abs(to_y - from_y);

	// Compute Signs
	sx = from_x < to_x ? 1 : -1;
	sy = from_y < to_y ? 1 : -1;

	// Compute Error
	err = (dx > dy ? dx : -dy) / 2;

	// Loop
	for(;;)
	{
		// Plot Pixel
		gfx_dsp_plot(dsp, from_x, from_y, c);

		// Break on End
		if((from_x == to_x) && (from_y == to_y))					{ return; }

		// Handle Error
		e2 = err;
		if(e2 > -dx)												{ err = err - dy; from_x = from_x + sx; }
		if(e2 < dy)													{ err = err + dx; from_y = from_y + sy; }
	}
}

// Draw Fill
void gfx_draw_fill(struct gfx_dsp *dsp, int x, int y, uint16_t w, uint16_t h, uint16_t c)
{
	uint16_t i;
	uint16_t j;

	// Clip Area
	if(gfx_dsp_clip(dsp, &x, &y, 0, 0, &w, &h))						{ return; }

	// Fill Area
	for(i = 0; i < h; i = i + 1)									{ for(j = 0; j < w; j = j + 1) { gfx_dsp_plot(dsp, x + j, y + i, c); } }
}

// Draw Rectangle
void gfx_draw_rect(struct gfx_dsp *dsp, int x, int y, int w, int h, uint16_t c)
{
	// Draw Rectangle (4 lines)
	gfx_draw_line(dsp, x, y, x + w, y, c);
	gfx_draw_line(dsp, x, y + h, x + w, y + h, c);
	gfx_draw_line(dsp, x, y, x, y + h, c);
	gfx_draw_line(dsp, x + w, y, x + w, y + h, c);
}

// Draw N-Gon (n = number of lines, varargs = x0, y0, x1, y1, x2, y2)
void gfx_draw_ngon(struct gfx_dsp *dsp, uint16_t c, uint8_t n, ...)
{
	va_list ap;

	// Acquire Args
	va_start(ap, n);

	// Draw N-Gon
	gfx_draw_ngon_v(dsp, c, n, ap);

	// Release Args
	va_end(ap);
}

// Draw N-Gon (variable argument list)
void gfx_draw_ngon_v(struct gfx_dsp *dsp, uint16_t c, uint8_t n, va_list ap)
{
	uint8_t i;
	int p_x;
	int p_y;
	int n_x;
	int n_y;

	// Loop
	for(i = 0; i < n; i = i + 1)
	{
		// Acquire Line Start
		if(i > 0)													{ p_x = n_x; p_y = n_y; }
		else														{ p_x = va_arg(ap, int); p_y = va_arg(ap, int); }

		// Acquire Next Point
		n_x = va_arg(ap, int);
		n_y = va_arg(ap, int);

		// Draw Line
		gfx_draw_line(dsp, p_x, p_y, n_x, n_y, c);
	}
}

// Draw Circle
void gfx_draw_circle(struct gfx_dsp *dsp, int x, int y, uint16_t r, uint16_t c)
{
	int nx;
	int ny;
	int dx;
	int dy;
	int err;

	// Prepare
	nx = r - 1;
	ny = 0;
	dx = 1;
	dy = 1;
	err = dx - (r << 1);

	// Loop
	while(nx >= ny)
	{
		// Draw all octants
		gfx_dsp_plot(dsp, x + nx, y + ny, c);
		gfx_dsp_plot(dsp, x + ny, y + nx, c);
		gfx_dsp_plot(dsp, x - ny, y + nx, c);
		gfx_dsp_plot(dsp, x - nx, y + ny, c);
		gfx_dsp_plot(dsp, x - nx, y - ny, c);
		gfx_dsp_plot(dsp, x - ny, y - nx, c);
		gfx_dsp_plot(dsp, x + ny, y - nx, c);
		gfx_dsp_plot(dsp, x + nx, y - ny, c);

		// Move
		if(err <= 0)
		{
			ny = ny + 1;
			err = err + dy;
			dy = dy + 2;
		}
		if(err > 0)
		{
			x = x - 1;
			dx = dx + 2;
			err = err + (-r << 1) + dx;
		}
	}
}
